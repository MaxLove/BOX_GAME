class Piece {
    constructor(x, y, height, width) {
        this.x = x;
        this.ay = y;
        this.width = width;
        this.height = height;
    }
}

function draw_to_canvas(canv, boardDim, pieces) {
  
}

function main() {
  
    let pieces = [];
  
    const boardDim = [500, 600];
    const playerPos = [Math.ceil(boardDim[0] / 2.0), Math.ceil(boardDim[1] / 2.0)]
    const gameCanv = document.getElementById("gameCanv");
    const canv2d = gameCanv.getContext("2d");
    const fps = 30;
  
    canv2d.width = boardDim[0];
    canv2d.height = boardDim[1];
  
    setInterval(draw_to_canvas, Math.ceil(1000 / 30.0), canv2d, boardDim, pieces);
    
    while (1) {
        
    }
}

document.addEventListener("DOMContentLoaded", main);
